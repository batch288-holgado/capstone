import {Container, Row, Col, Button, Card, Form} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import UserContext from '../UserContext.js'
import AdminProductProp from '../pages/Products.js'
import Swal2 from 'sweetalert2'

export default function AdminProductCard(props) {

	const [isDisabled, setIsDisabled] = useState(true)
	const [isStockDisabled, setIsStockDisabled] = useState(true)
	const [updatedDescription, setUpdatedDescription] = useState('')
	const [updatedIsAvailable, setUpdatedIsAvailable] = useState(true)
	const [updatedStock, setUpdatedStock] = useState('')

	const {_id, name, description, isAvailable, createdOn, price, stock} = props.AdminProductProp

	useEffect(()=>{
		if (updatedDescription !== ''){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}
	}, [updatedDescription])

	useEffect(()=>{
		if (updatedStock !== ''){
			setIsStockDisabled(false)
		}else{
			setIsStockDisabled(true)
		}
	}, [updatedStock])

	function updateDescription (event) {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/updateProductDescription`, {
			method: 'PATCH',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				productId: _id,
				description: updatedDescription
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data){
				Swal2.fire({
					title: 'Update Successful!',
					icon: 'success',
					text: 'Product description was successfully updates'
				}).then(() => {window.location.reload()})
			}
		})
	}

	function archiveProduct (event) {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/archiveProduct`, {
			method: 'PATCH',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				productId: _id
			})
		})
		.then(result=>result.json())
		.then(data=> {
			if (data===true) {
				Swal2.fire({
					title: 'Item successfully archived!',
					icon: 'success',
					text: 'Happy Shopping'
				}).then(()=>{window.location.reload()})
			}else{
				Swal2.fire({
					title: 'Item is already archived',
					icon: 'error',
					text: 'Please choose a different item'
				})
			}
		})
	}

	function unarchiveProduct (event) {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/unarchiveProduct`, {
			method: 'PATCH',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				productId: _id
			})
		})
		.then(result=>result.json())
		.then(data=> {
			if (data===true) {
				Swal2.fire({
					title: 'Item retrieved from archive!',
					icon: 'success',
					text: 'Happy Shopping'
				}).then(()=>{window.location.reload()})
			}else{
				Swal2.fire({
					title: 'Item is already available',
					icon: 'error',
					text: 'Please choose a different item'
				})
			}
		})
	}

	const updateStock = (event) => {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/updateStock`, {
			method: 'PATCH',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				productId: _id,
				stock: updatedStock
			})
		})
		.then(result => result.json())
		.then(data => {
			if (data) {
				Swal2.fire({
					title: 'Stock successfully updated!',
					icon: 'success',
					text: 'Thank you admin!',
					willClose: () => {
						window.location.reload();
					}
				})
			}else{
				Swal2.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}

	return(
	
			<Col className = 'col-3 mb-3 mx-auto'>
				<Card>
					<Card.Body className = 'text-center'>
							
						<Card.Title>{name}</Card.Title>
							
						<Card.Subtitle className = 'mt-3' style={{ fontWeight: 'bold' }}>Current Description</Card.Subtitle>
							
						<Card.Text>
						{description}
						</Card.Text>

						<Form onSubmit = {event => updateDescription(event)}>

							<Form.Group className="mb-3" controlId="formUpdateDescription">
								<Form.Label style={{ fontWeight: 'bold' }}>Update Description</Form.Label>
								<Form.Control 
									type="description" 
									placeholder="input updated description" 
									value = {updatedDescription}
									onChange = {event => setUpdatedDescription(event.target.value)}
								/>
							</Form.Group>

							<div className="d-grid">
								<Button id = "updateDescriptionButton" type="submit" className="text-center mx-auto" disabled = {isDisabled}>
								Update
								</Button>
							</div>

						</Form>

						<Card.Subtitle className = 'mt-3' style={{ fontWeight: 'bold' }}>Current Availability</Card.Subtitle>
							
						<Card.Text>
						{JSON.stringify(isAvailable)}
						</Card.Text>

						<div className="d-flex justify-content-center">
							<div>
								<Button id = "archiveProductButton" type="archive" className = 'me-1' onClick = {event => archiveProduct(event)}>
								Archive
								</Button>
							</div>

							<div>
								<Button id = "unarchiveProductButton" type="archive" onClick = {event => unarchiveProduct(event)}>
								Unarchive
								</Button>
							</div>
						</div>

						<Card.Subtitle className = 'mt-3' style={{ fontWeight: 'bold' }}>Current Stock</Card.Subtitle>
							
						<Card.Text>
						{stock}
						</Card.Text>

						<Form onSubmit = {event => updateStock(event)}>

							<Form.Group className="mb-3" controlId="formUpdateStock">
								<Form.Label style={{ fontWeight: 'bold' }}>Update Stock</Form.Label>
								<Form.Control 
									type="number" 
									placeholder="input updated stock" 
									value = {updatedStock}
									onChange = {event => setUpdatedStock(event.target.value)}
								/>
							</Form.Group>

							<div className="d-grid">
								<Button id = "updateStockButton" type="submit" className="text-center mx-auto" disabled = {isStockDisabled}>
								Update
								</Button>
							</div>

						</Form>

					</Card.Body>
				</Card>
			</Col>
	)
}