const express = require('express')
const router = express.Router()
const productsControllers = require('../controllers/productsControllers.js')
const auth = require('../auth.js')

router.post('/createProduct', productsControllers.createProduct)
router.get('/retrieveAllProducts', productsControllers.retrieveAllProducts)
router.get('/retrieveAvailableProducts', productsControllers.retrieveAvailableProducts)
router.get('/retrieveUnavailableProducts', productsControllers.retrieveUnavailableProducts)
router.get('/:productId', productsControllers.retrieveSingleProduct)
router.patch('/updateProductDescription', productsControllers.updateProductDescription)
router.patch('/archiveProduct', productsControllers.archiveProduct)
router.patch('/unarchiveProduct', productsControllers.unarchiveProduct)
router.patch('/updateStock', productsControllers.updateStock)
router.patch('/:productId/updatePrice', auth.verify, productsControllers.updatePrice)

module.exports = router