const Products = require ('../models/Products.js')
const auth = require ('../auth.js')

module.exports.createProduct = (request, response) => {

		let newProduct = new Products({
			name: request.body.name,
			description: request.body.description,
			isAvailable: request.body.isAvailable,
			stock: request.body.stock,
			price: request.body.price
		})

		newProduct.save()
		.then(saved => response.send(true))
		.catch(error => response.send(false))
}

module.exports.retrieveAllProducts = (request, response) => {
	Products.find({})
	.then(result => response.send(result))
	.catch(error => response.send(error))	
}

module.exports.retrieveAvailableProducts = (request, response) => {
	Products.find({isAvailable: true})
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

module.exports.retrieveUnavailableProducts = (request, response) => {
	Products.find({isAvailable: false})
	.then(result => response.send(result))
	.catch(error => response.send(error))	
}

module.exports.retrieveSingleProduct = (request, response) => {
	const productId = request.params.productId 
	Products.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

module.exports.updateProductDescription = (request, response) => {

	const productId = request.body.productId

	let updateDescription = {
		description: request.body.description
	}

	Products.findByIdAndUpdate(productId, updateDescription)
	.then(result => response.send(true))
	.catch(error => response.send(false))

}

module.exports.archiveProduct = (request, response) => {

	const productId = request.body.productId
	let archiveProduct = {
		isAvailable: false
	}

	Products.findByIdAndUpdate(productId, archiveProduct)
	.then(result => {
		if (result.isAvailable){
			return response.send(true)
		}else{
			return response.send(false)
		}
	})
	.catch(error => response.send(error))

}

module.exports.unarchiveProduct = (request, response) => {

	const productId = request.body.productId
	let unarchiveProduct = {
		isAvailable: true
	}

	Products.findByIdAndUpdate(productId, unarchiveProduct)
	.then(result => {
		if (!result.isAvailable){
			return response.send(true)
		}else{
			return response.send(false)
		}
	})
	.catch(error => response.send(error))
}

module.exports.updateStock = (request, response) => {

	const productId = request.body.productId
	let updateStock = {
		stock: request.body.stock
	}

	Products.findByIdAndUpdate(productId, updateStock)
	.then(result => {
		response.send(true)
	})
	.catch(error => response.send(false))

}

module.exports.updatePrice = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	let updatePrice = {
		price: request.body.price
	}

	if(userData.isAdmin){

		Products.findByIdAndUpdate(productId, updatePrice)
		.then(result => response.send(`${result.name}'s price has been updated`))
		.catch(error => response.send(error))

	}else{
		return response.send('Token provided is not an admin. You do not have access to this route.')
	}
}