import {Container, Row, Col} from 'react-bootstrap'
import {useEffect} from 'react'
import {Link} from 'react-router-dom'

export default function notFound () {

	useEffect(()=>{
		localStorage.clear()
	})

	return(
		<div>
			<img id='notFoundImage' src='https://colorlib.com/wp/wp-content/uploads/sites/2/404-error-template-3.png'/>
			<p className="text-center"> Go back to <Link to = '/' style = {{textDecoration: 'none'}}>homepage</Link></p>
		</div>
	)
}