import {Container, Row, Col, Button, Card} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Link, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext.js'
import ProductProp from '../pages/Products.js'
import Swal2 from 'sweetalert2'

export default function UserProductCard(props) {

	const {user, setUser} = useContext(UserContext)

	const {_id, name, description, isAvailable, createdOn, price} = props.UserProductProp

	const navigate = useNavigate()

	const details = (event) => {
		event.preventDefault()
		if (user.isAdmin===true) {
			Swal2.fire({
				title: 'Admin cannot order products',
				icon: 'info',
				text: 'Please choose a different task'
			})
		}else{
			navigate(`/products/${_id}`)
		}
	}

	return(
			<Col className = 'col-md-4 col-sm-12 mb-3 mx-auto text-center'>
				<Card>
					<Card.Body>
						<img className = 'shoes' src={process.env.PUBLIC_URL + `/images/${_id}.jpg`} alt='No image provided'/>

						<Card.Title>{name}</Card.Title>
							
						<Card.Subtitle className = 'mt-3'>Description:</Card.Subtitle>
							
						<Card.Text>
						{description}
						</Card.Text>

						<Card.Subtitle className = 'mt-3 mb-2'>Price: Php {price}</Card.Subtitle>

						{
							user.id !== null 
							?
							<Button id = 'productDetailsButton' as = {Link} onClick = {event => details(event)}>Details</Button>
							// this ${_id} will be retrieved using useParams in CourseView
							:
							<Button id = 'loginToOrderButton' as = {Link} to = '/login'>Login to Order</Button>
						}

					</Card.Body>
				</Card>
			</Col>
	)
}