let text = []

function read (index) {
	console.log(text[index])
}

function create (pencil, color, message) {

	if (text.length > 4) {

		console.log('Text input no longer available')

	}else if (pencil === 'Graphite' || pencil === 'Colored'){

		if (pencil === 'Graphite' && message.length > 50){

			console.log('Graphite pencil has run out')

		}else if(pencil === 'Graphite' && color === 'none' && message.length < 50){

			text.push(message)
			console.log('Graphite pencil was used. ' + (50-message.length) + ' characters left available.')

		}else if(pencil === 'Graphite' && color !== 'none'){

			console.log('Color not available for graphite pencil')

		}else if (pencil === 'Colored' && message.length > 30){

			console.log('Colored pencil has run out')

		}else if(color==="red" || color==="blue" || color==="green" && message.length < 30){
			
			text.push(message)
			console.log('Color ' + color + ' was used. ' + (30-message.length) + ' characters left available.')

		}else if(color !== "red" || color !== "blue" || color !== "green"){

			console.log('Color not available. Please choose red, blue or green only.')

		}

	}else{
		console.log('Pencil not available')
	}
	
} 

/*for(let i=0; i<message.length; i++){
	if(message[i] === " "){
		message.length++
	}
}*/

create('Graphite', 'none', 'write with graphite write with graphite write with graphite')
create('Colored', 'red', 'write with red')
create('Colored', 'green', 'write with green')
create('Colored', 'blue', 'write with blue write with blue write with blue')
/*create('Colored', 'green', 'write with green pencils')*/
/*create('Colored', 'green', 'write with green pencil')
create('Colored', 'blue', 'write with blue pencil')
create('Colored', 'red', 'write with red pencil')
create('Colored', 'red', 'write with red pencil')
create('Colored', 'red', 'write with red pencil')*/

function update (index, updatedMessage) {
	text.splice(index, updatedMessage)
}

function remove (num) {
	text.slice(num)
}